from django.urls import path , include
from . import views

urlpatterns = [
    path('' , views.index),
    path('detail-view/<int:pk>' , views.detail_view , name='detail'),
    path('delete-flower/<int:pk>' , views.delete_flower , name='delete'),
    path('delete-all' , views.delete_all , name='delete-all'),
    path('create-new' , views.create_new , name='create-new'),
]



