from django.contrib import admin
from .models import Flower , TypeFlower

# Register your models here.

class FlowerAdmin(admin.ModelAdmin):
    list_display = ['name' , 'price']
    list_filter = ('price' ,)

admin.site.register(Flower , FlowerAdmin)
admin.site.register(TypeFlower)




