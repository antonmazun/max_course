from django.db import models


# Create your models here.

class TypeFlower(models.Model):
    name = models.CharField(max_length=100, verbose_name='Тип цветка')

    def __str__(self):
        return self.name


class Flower(models.Model):
    type_flower = models.ForeignKey(TypeFlower, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, verbose_name='Название цветка')
    price = models.FloatField(verbose_name='Цена за шт')
    height = models.FloatField(verbose_name='Высота стебля (м)')
    image = models.ImageField(upload_to='flowers_images', verbose_name='Фотография',
                              default='')

    def __str__(self):
        return self.name + ' ' + str(self.price) + ' грн'

    class Meta:
        verbose_name = 'Цветок'
        verbose_name_plural = 'Все цветы'

# class Flower:
#     color
#     price
#     height
#     image
#     type
#     country
