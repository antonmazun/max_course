from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Flower, TypeFlower


# Create your views here.
# #
# import math as M
# #
# # M.sin(34)
#
# import math
# M = math
# del math
#
# M.sin(45)


def index(request):
    ctx = {}
    x = Flower.objects.all()  # SELECT * FROM Flower;
    ctx['all_flowers'] = x
    return render(request, 'index.html', ctx)


def detail_view(request, pk):
    ctx = {}
    ctx['all_flowers'] = Flower.objects.all()
    ctx['flower_object'] = Flower.objects.get(id=pk)
    return render(request, 'flowers/detail_view.html', ctx)


def delete_flower(request, pk):
    ctx = {}
    try:
        Flower.objects.get(id=pk).delete()
        x = Flower.objects.all()  # SELECT * FROM Flower;
        ctx['all_flowers'] = x
        ctx['deleted'] = True
    except Exception as e:
        ctx['error'] = e
    return render(request, 'index.html', ctx)


def delete_all(request):
    Flower.objects.all().delete()
    return redirect('/')


def create_new(request):
    ctx = {}
    ctx['all_types'] = TypeFlower.objects.all()
    template_path = 'flowers/create_new.html'
    if request.method == 'GET':
        return render(request, template_path, ctx)
    elif request.method == 'POST':
        try:
            Flower.objects.create(
                type_flower=TypeFlower.objects.get(id=request.POST.get('type_flower')),
                name=request.POST.get('name'),
                price=request.POST.get('price'),
                height=request.POST.get('height')
            )
            ctx['added'] = True
        except Exception as e:
            ctx['error'] = e
        return render(request, template_path, ctx)
