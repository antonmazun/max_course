# Generated by Django 2.1.7 on 2019-03-13 10:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('flowers', '0002_auto_20190307_1129'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='flower',
            options={'verbose_name': 'Цветок', 'verbose_name_plural': 'Все цветы'},
        ),
        migrations.AddField(
            model_name='flower',
            name='image',
            field=models.ImageField(default='', upload_to='flowers_images', verbose_name='Фотография'),
        ),
    ]
